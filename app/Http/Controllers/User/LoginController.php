<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginValidate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __invoke()
    {
        return view('user.login');
    }

    public function submitForm(LoginValidate $request)
    {
        $userdata = array(
            'username' => $request->username,
            'password' => $request->password
        );

        // attempt to do the login
        if (Auth::attempt($userdata)) {

            $user = User::where('username', $request->username)->first();

            Auth::loginUsingId($user->id);

            return redirect()->route('post.all');
        }

        return redirect()->back()->with(['status' => 'danger', 'message' => 'Credential mismatch.']);
    }
    protected function guard()
    {
        return Auth::guard();
    }
}
