<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{

    public function __invoke()
    {
        return view('user.register');
    }

    public function submitForm(ValidateUser $request)
    {
        User::Create([
            'name' => $request->name,
            'username' => $request->username,
            'password' => Hash::make($request->password)
        ]);

        return redirect()->back()->with(['status' => 'success', 'message' => 'You have successfully registered with us.']);
    }
}
