<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CreatePost;
use App\Http\Requests\UpdatePost;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request  $request)
    {
        $user = $request->user();
        $categories = Category::pluck('name', 'id');
        $posts = $user->posts()->paginate();
        $post = new Post();

        return view('post.all', ['categories' => $categories, 'posts' => $posts, 'post' => $post]);
    }

    public function store(CreatePost $request)
    {
        $post = new Post();

        $post->name = $request->name;
        $post->category()->associate($request->category);
        $post->user()->associate($request->user()->id);

        if($request->hasFile('image')) {
          $post->image =  $request->image->store(Post::IMAGE_PATH, 'public');
        }

        $post->save();

        return redirect()->route('post.all')->with(['status' => 'success', 'message' => 'Post successfully created.']);
    }

    public function edit($id, Request  $request)
    {
        $user = $request->user();
        $post = $user->posts()->findOrFail($id);
        $categories = Category::pluck('name', 'id');
        $posts = $user->posts()->paginate();

        return view('post.all', ['categories' => $categories, 'posts' => $posts, 'post' => $post]);
    }

    public function update($id, UpdatePost $request)
    {
        $user = $request->user();

        $post = $user->posts()->findOrFail($id);

        $post->name = $request->name;
        $post->category()->associate($request->category);

        if($request->hasFile('image')) {
            $post->image =  $request->image->store(Post::IMAGE_PATH, 'public');
        }

        $post->save();

        return redirect()->route('post.all')->with(['status' => 'success', 'message' => 'Post successfully updated.']);
    }

    public function show($id, Request  $request)
    {
        $user = $request->user();
        $post = $user->posts()->with('comments')->findOrFail($id);

        return view('post.show', ['post' => $post]);
    }
}
