<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function submit($id, Request $request)
    {
       try {

           $post = Post::find($id);

           if(!$post) {
               throw new \Exception('Post not found');
           }

           $user = $request->user();

           $comment = new Comment();
           $comment->comment = $request->comment;
           $comment->user()->associate($user->id);
           $comment->post()->associate($id);
           $comment->save();

           $response = view('post.comment', ['comment' => $comment])->render();

           return ['comment' => $response, 'total' => $post->comments()->count()];

       }catch (\Exception $e)
       {
           return response()->json(['status' => 'fail', 'message' => $e->getMessage()]);
       }
    }
}
