<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    const IMAGE_PATH = 'post/image';

    protected $fillable = [ 'name', 'image' ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getImage()
    {
        return Storage::url($this->image);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
