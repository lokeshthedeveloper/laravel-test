@extends('layout.main')
@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-6 col">
            <h4>Register</h4>
            {!! Form::open(['route' => 'user.register.submit']) !!}
                @if(session()->get('status'))
                    <div class="alert alert-success">{{ session()->get('message') }}</div>
                @endif
                <div class="form-group">
                    <label for="formGroupExampleInput">Name</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Name" name="name" value="{{ old('name') }}">
                    <span class="alert-danger">
                         {!! $errors->first('name', ':message') !!}
                    </span>
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Username</label>
                    <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Username" name="username" value="{{ old('username') }}">
                    <span class="alert-danger">
                         {!! $errors->first('username', ':message') !!}
                    </span>
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Password</label>
                    <input type="password" class="form-control" id="formGroupExampleInput2" placeholder="Password" name="password" value="">
                    <span class="alert-danger">
                         {!! $errors->first('name', ':password') !!}
                    </span>
                </div>
                <input type="submit" name="submit" value="Submit" class="btn btn-primary">
            </form>
        </div>
    </div>

@endsection
