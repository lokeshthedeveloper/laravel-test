@extends('layout.main')
@section('content')
    <div class="row {{ !$posts->count() ? 'justify-content-md-center' : '' }}">
        <div class="col-md-6 col">
            <h4>{{ $post->id ? 'Update' : 'Create' }} Post</h4>
            @if($post->id)
                {!! Form::model($post, ['route' => ['post.update', $post->id], 'files' => true]) !!}
                {!! method_field('PUT') !!}
            @else
            {!! Form::open(['route' => 'post.submit', 'files' => true]) !!}
            @endif
            @if(session()->get('status'))
                <div class="alert alert-success">{{ session()->get('message') }}</div>
            @endif
            <div class="form-group">
                <label for="formGroupExampleInput2">Post Name</label>
                <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Post Name" name="name" value="{{ old('name', $post->name) }}">
                <span class="alert-danger">
                         {!! $errors->first('name', ':message') !!}
                    </span>
            </div>

            <div class="form-group">
                <label for="formGroupExampleInput2">Category</label>
                {!! Form::select('category', $categories, old('category', $post->category_id), ['class' => 'form-control input']) !!}
                <span class="alert-danger">
                         {!! $errors->first('category', ':message') !!}
                    </span>
            </div>

            <div class="form-group">
                <label for="formGroupExampleInput2">Image</label>
                <input type="file" class="form-control" id="formGroupExampleInput2" name="image">
                <span class="alert-danger">
                         {!! $errors->first('image', ':message') !!}
                    </span>
            </div>

            <input type="submit" name="submit" value="Submit" class="btn btn-primary">

            @if($post->id)
                <a href="{{ route('post.all') }}" class="btn btn-dark">Create New Post</a>
            @endif
            </form>
        </div>

        @if($posts->count())
        <div class="col-md-6">
            <h4>All Post ({{ $posts->total() }})</h4>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Image</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->name }}</td>
                        <td>{{ $post->category->name }}</td>
                        <td><img src="{{ $post->getImage() }}" style="height: 50px;"></td>
                        <td>
                            <a href="{{ route('post.edit', $post->id) }}" class="btn btn-sm btn-primary">Edit</a>
                            <a href="{{ route('post.show', $post->id) }}" class="btn btn-sm btn-dark">View</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $posts->links() !!}
        </div>
         @endif

    </div>

@endsection
