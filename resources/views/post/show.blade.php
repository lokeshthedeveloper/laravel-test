@extends('layout.main')
@section('content')
    <div class="row justify-content-md-center">
        <div class="col-md-7 col">
            <h4>Post: {{ $post->name }}</h4>
            <h6>Category: {{ $post->category->name }}</h6>
            <img class="img img-responsive" src="{{ $post->getImage() }}" />

            <hr>
            <h5>Comments (<span id="totalComment">{{ $post->comments()->count() }}</span>)</h5>
            {!! Form::open(['class' => 'form-inline', 'id' => 'ajax-form']) !!}
            <div class="row">
                <div class="form-group">
                    <label for="inputPassword2" class="sr-only">Comment</label>
                    <input type="text" class="form-control" id="comment" placeholder="Comment">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Submit</button>
            </div>
            {!! Form::close() !!}

            <hr>

            @if($post->comments()->count())
                <div id="comments">
                    @foreach($post->comments as $comment)
                        @include('post.comment', ['comment' => $comment])
                    @endforeach
                </div>
            @endif

        </div>
    </div>

    <script type="text/javascript">

        jQuery("#ajax-form").on('submit', function (e){
           e.preventDefault();

            let $this = jQuery(this);
            let postId = '{{ $post->id }}';
            let comment = $this.find('#comment').val();


            jQuery.ajax({
                'method' : 'post',
                'url' : '/comment/submit/' + postId,
                dataType: 'json',
                data: { 'comment': comment },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    jQuery("#comments").append(response.comment);
                    jQuery("#totalComment").text(response.total);
                },
                error(err) {
                    console.log(err);
                }
            })

        });


    </script>

@endsection
